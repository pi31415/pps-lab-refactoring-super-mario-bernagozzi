package characters;

import java.awt.Image;

import objects.GameObjects;

public interface CharactersInterface {
	public Image walk(String name , int frequency );
	public boolean contact_A_Destra(GameObjects ogetto);
}
