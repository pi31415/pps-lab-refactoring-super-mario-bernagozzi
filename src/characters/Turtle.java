package characters;

import java.awt.Image;

import javax.swing.ImageIcon;

import objects.GameObjects;

public class Turtle extends Characters implements Runnable {
	
	private Image imgTurtle;
	private ImageIcon icoTurtle;
	
	private final int PAUSE = 15;
	private int dxTurtle; // passo spostamento turtle
	
	public Turtle(int X, int Y) {
		super(X, Y,43 , 50);
		super.setMovingToRight(true);
		super.setMoving(true);
		this.dxTurtle = 1;
		this.icoTurtle = new ImageIcon(getClass().getResource("/images/turtleAD.png"));
		this.imgTurtle = icoTurtle.getImage();
		
		Thread chronoTurtle = new Thread(this);
		chronoTurtle.start();
	}

	//getters
	public Image getImgTurtle() {
		return imgTurtle;
	}
	
	//metodi
	
		public void muoviti(){ //spostamento turle
			if(super.isMovingToRight() == true){this.dxTurtle =1;}
			else{this.dxTurtle= -1; }
			super.setInitialXPosition(super.getInitialXPosition()+this.dxTurtle);
			
		}
	
		@Override
		public void run() {
			try{Thread.sleep(20);}
			catch(InterruptedException e){}
			
			while(true){
				if(this.isAlive == true){
					this.muoviti();
					try{Thread.sleep(PAUSE);}
					catch(InterruptedException e){}
					}
				}
		}
		
		public void contact (GameObjects obj){
			if(super.contactOnTheRight(obj) == true && this.isMovingToRight() == true){
				super.setMovingToRight(false);
				this.dxTurtle = -1;
			}else if (super.contactOnTheLeft(obj)==true && this.isMovingToRight() ==false){
				super.setMovingToRight(true);
				this.dxTurtle = 1;
			}
		}
		
		public void contact (Characters pers){
			if(super.contactOnTheRight(pers) == true && this.isMovingToRight() == true){
				super.setMovingToRight(false);
				this.dxTurtle = -1;
			}else if (super.contactOnTheLeft(pers)==true && this.isMovingToRight() ==false){
				super.setMovingToRight(true);
				this.dxTurtle = 1;
			}
		}
		 
		public Image muore(){		
				String str = "/images/turtleF.png";
				ImageIcon ico;
				Image img;		
		        ico = new ImageIcon(getClass().getResource(str));
		        img = ico.getImage();
				return img;
		}
}
