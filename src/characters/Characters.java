package characters;

import java.awt.Image;

import javax.swing.ImageIcon;

import game.Main;
import objects.GameObjects;

public class Characters {

	private int width , height; // dimensioni largezza e altezza
	private int initialXPosition, initialYPosition; // posizione iniziale
	protected boolean isMoving; // vero quando il personnagio camina
	protected boolean isMovingToRight; // vero quando camina verso la destro false verso la sinistra
	public int stepFrequencyCounter; // frequenza passi effetuati
	protected boolean isAlive;
	
	
	public Characters(int X , int Y , int L , int H){
		
		this.initialXPosition = X ;
		this.initialYPosition = Y ;
		this.height = H ;
		this.width = L ;
		this.stepFrequencyCounter = 0;
		this.isMoving = false ;
		this.isMovingToRight = true ;
		this.isAlive = true;
	}


	// getters
	
	public boolean isAlive() {
		return isAlive;
	}

	public int getL() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	public int getInitialXPosition() {
		return initialXPosition;
	}
	public int getInitialYPosition() {
		return initialYPosition;
	}
	public boolean isMoving() {
		return isMoving;
	}
	public boolean isMovingToRight() {
		return isMovingToRight;
	}
	public int getStepFrequencyCounter() {
		return stepFrequencyCounter;
	}

	// setters 

	public void setAlive(boolean alive) {
		this.isAlive = alive;
	}

	public void setInitialXPosition(int initialXPosition) {
		this.initialXPosition = initialXPosition;
	}
	public void setInitialYPosition(int initialYPosition) {
		this.initialYPosition = initialYPosition;
	}
	public void setMoving(boolean moving) {
		this.isMoving = moving;
	}
	public void setMovingToRight(boolean movingToRight) {
		this.isMovingToRight = movingToRight;
	}
	public void setStepFrequencyCounter(int stepFrequencyCounter) {
		this.stepFrequencyCounter = stepFrequencyCounter;
	}
	
	//metodo per gestire i movimenti dei personagi
	
	public Image walk(String nome , int frequenza ){
		
		String str ;
		ImageIcon ico ;
		Image img ; 
		
		if(this.isMoving == false ){
			
			if(this.isMovingToRight == true){ // se guarda a destra
				str = "/images/" + nome + "AD.png";
			}else str = "/images/" + nome + "AG.png"; //se non
		}else {
			this.stepFrequencyCounter++;
			if(this.stepFrequencyCounter / frequenza == 0){ //
				if(this.isMovingToRight == true){
					str = "/images/" + nome + "AD.png"; // mario fermo a destra
				}else str = "/images/" + nome + "AG.png";// mario fermo a sinistra
			}else{
				if(this.isMovingToRight == true){
					str = "/images/" + nome + "D.png"; // mario che camina verso destra
				}else str = "/images/" + nome + "G.png";// mario che camina verso sinistra
			}
			if(this.stepFrequencyCounter == 2* frequenza )
				this.stepFrequencyCounter = 0 ;
		}
		
		//imagine del personnagio 
		ico = new ImageIcon(getClass().getResource(str));
		img = ico.getImage();
		
		return img;
	}
	
	public void movements(){
		if(Main.scene.getxPos() >= 0){
			this.initialXPosition = this.initialXPosition - Main.scene.getMov();
		}
	}
	


/*	@Override
	public boolean contact_A_Destra(GameObjects ogetto) {
		
		if(this.isMovingToRight == true){
			if(this.initialXPosition + this.l < ogetto.getInitialXPosition() || this.initialXPosition + this.l > ogetto.getInitialXPosition() + 5 ||
					this.initialYPosition + this.height <= ogetto.getInitialYPosition() || this.initialYPosition >= ogetto.getInitialYPosition() + ogetto.getHeight()){
				return false;
			}else{
				return true;
			}
		}else return false;
	}*/
	
	// detezione contact a destra
	public boolean contactOnTheRight(GameObjects og){
		if(this.initialXPosition + this.width < og.getX() || this.initialXPosition + this.width > og.getX() + 5 ||
				this.initialYPosition + this.height <= og.getY() || this.initialYPosition >= og.getY() + og.getH() ){
			return false;
		}else
			return true;		
	}
	
	// detezione contact a sinistra 
	protected boolean contactOnTheLeft(GameObjects og){
		if(this.initialXPosition > og.getX() + og.getL() ||this.initialXPosition + this.width < og.getX() + og.getL() -5 ||
			this.initialYPosition + this.height <= og.getY() || this.initialYPosition >= og.getY() + og.getH() ){
			return false;
		}else
			return true;		
	}
	
	//contact sotto
	protected boolean contactDown(GameObjects og){
		if(this.initialXPosition + this.width < og.getX() + 5 || this.initialXPosition > og.getX() + og.getL() - 5 ||
			this.initialYPosition + this.height < og.getY() || this.initialYPosition +this.height > og.getY() + 5 ){
			return false;
		}else
			return true;		
	}
	
	// contact in alto
	protected boolean contactUp(GameObjects og){
		if(this.initialXPosition + this.width < og.getX() + 5 || this.initialXPosition > og.getX() + og.getL() - 5 ||
			this.initialYPosition < og.getY() + og.getH() || this.initialYPosition > og.getY()+ og.getH() +5 ) {
			return false;
		}else return true;				
	}
	
	public boolean vicino(GameObjects obj){
		
		if((this.initialXPosition > obj.getX() - 10 && this.initialXPosition < obj.getX() + obj.getL() + 10) ||
			(this.getInitialXPosition()+ this.width > obj.getX() - 10 && this.initialXPosition + this.width < obj.getX() + obj.getL() + 10)) {
				return true;
		}else return false ;
		
	}
	
	//contact fra personnaggi
	protected boolean contactOnTheRight(Characters pers){
		if(this.isMovingToRight() == true){
			if(this.initialXPosition + this.width < pers.getInitialXPosition() || this.initialXPosition + this.width > pers.getInitialXPosition() + 5 ||
					this.initialYPosition + this.height <= pers.getInitialYPosition() || this.initialYPosition >= pers.getInitialYPosition() + pers.getHeight()){
				return false;
				}
			else{return true;}
		}else{return false;}
	}
	
	protected boolean contactOnTheLeft(Characters pers){
		if(this.initialXPosition > pers.getInitialXPosition() + pers.getL() || this.initialXPosition + this.width < pers.getInitialXPosition() + pers.getL() - 5 ||
				this.initialYPosition + this.height <= pers.getInitialYPosition() || this.initialYPosition >= pers.getInitialYPosition() +pers.getHeight()){
			return false;
			}
		else{return true;}
	}
	
	public boolean contactDown(Characters pers){
		if(this.initialXPosition + this.width < pers.getInitialXPosition() || this.initialXPosition > pers.getInitialXPosition() + pers.getL() ||
				this.initialYPosition + this.height < pers.getInitialYPosition() || this.initialYPosition + this.height > pers.getInitialYPosition()){
			return false;
		}
		else{return true;}
	}
	//vicino fra personnagi
	public boolean vicino(Characters pers){
		if((this.initialXPosition > pers.getInitialXPosition() - 10 && this.initialXPosition < pers.getInitialXPosition() + pers.getL() + 10)
		    	|| (this.initialXPosition + this.width > pers.getInitialXPosition() - 10 && this.initialXPosition + this.width < pers.getInitialXPosition() +pers.getL() + 10)){
			return true;
			}
		    	else{return false;}
	}
}
