package characters;

import java.awt.Image;

import javax.swing.ImageIcon;

import objects.GameObjects;

public class Mushrooms extends Characters implements Runnable{

	private Image imgFunghi;
	private ImageIcon icoFunghi;
	
	private final int PAUSE = 15;
	private int dxFunghi;
	
	public Mushrooms(int X, int Y) {
		super(X, Y,27 , 30);
		super.setMovingToRight(true);
		super.setMoving(true);
		this.dxFunghi = 1;
		this.icoFunghi = new ImageIcon(getClass().getResource("/images/mushroomAliveRightStepClosed.png"));
		this.imgFunghi = icoFunghi.getImage();
		
		Thread chronoFunghi = new Thread(this);
		chronoFunghi.start();
	}

	//getters
	public Image getImgFunghi() {
		return imgFunghi;
	}

	
	//setters
	
	
	//metodi
	
	public void muoviti(){
		if(super.isMovingToRight() == true){this.dxFunghi =1;}
		else{this.dxFunghi= -1; }
		super.setInitialXPosition(super.getInitialXPosition()+this.dxFunghi);
		
	}
	
	@Override
	public void run() {
		try{Thread.sleep(20);}
		catch(InterruptedException e){}
		
		while(true){
			if(this.isAlive == true){
				this.muoviti();
				try{Thread.sleep(PAUSE);}
				catch(InterruptedException e){}
			}
		}
	}
	
	public void contact (GameObjects obj){
		if(super.contactOnTheRight(obj) == true && this.isMovingToRight() == true){
			super.setMovingToRight(false);
			this.dxFunghi = -1;
		}else if (super.contactOnTheLeft(obj)==true && this.isMovingToRight() ==false){
			super.setMovingToRight(true);
			this.dxFunghi = 1;
		}
	}
	
	public void contact (Characters pers){
		if(super.contactOnTheRight(pers) == true && this.isMovingToRight() == true){
			super.setMovingToRight(false);
			this.dxFunghi = -1;
		}else if (super.contactOnTheLeft(pers)==true && this.isMovingToRight() ==false){
			super.setMovingToRight(true);
			this.dxFunghi = 1;
		}
	}
	
	  public Image muore(){		
			String str;	
			ImageIcon ico;
			Image img;
	        if(this.isMovingToRight() == true){str = "/images/mushroomDeadRight.png";}
	        else{str = "/images/mushroomDeadLeft.png";}
	        ico = new ImageIcon(getClass().getResource(str));
	        img = ico.getImage();
			return img;
		}
	

	
}
