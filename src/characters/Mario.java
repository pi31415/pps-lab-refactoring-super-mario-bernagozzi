package characters;

import java.awt.Image;

import javax.swing.ImageIcon;

import game.Main;
import objects.GameObjects;
import objects.Piece;

public class Mario extends Characters {

	private ImageIcon Icopilar ;
	private Image imgpilar ;
	private boolean salto;
	private int compteurSaut ; //durata e intensit� del salto
	
	public Mario(int X, int Y) {
		super(X, Y, 28 , 50);
		Icopilar = new ImageIcon(getClass().getResource("/images/marioRightStepOpen.png"));
		this.imgpilar = this.Icopilar.getImage();
		this.salto = false;
		this.compteurSaut = 0;
	}

	//getters
	public boolean isSalto() {
		return salto;
	}

	//seters
	public void setSalto(boolean salto) {
		this.salto = salto;
	}

	public Image getImgpilar() {
		return imgpilar;
	}

	// metodi
	@Override
public Image walk(String nome , int frequenza ){
		
		String str ;
		ImageIcon ico ;
		Image img ; 
		
		if(this.isMoving == false || Main.scene.getxPos() <=0 || Main.scene.getxPos() > 4600){ //se non si muove o � completamente in fondo a sinistra
			
			if(this.isMovingToRight == true){ // se guarda a destra
				str = "/images/" + nome + "AD.png";
			}else str = "/images/" + nome + "AG.png"; //se non
		}else {
			this.stepFrequencyCounter++;
			if(this.stepFrequencyCounter / frequenza == 0){ //
				if(this.isMovingToRight == true){
					str = "/images/" + nome + "AD.png"; // mario fermo a destra
				}else str = "/images/" + nome + "AG.png";// mario fermo a sinistra
			}else{
				if(this.isMovingToRight == true){
					str = "/images/" + nome + "D.png"; // mario che camina verso destra
				}else str = "/images/" + nome + "G.png";// mario che camina verso sinistra
			}
			if(this.stepFrequencyCounter == 2* frequenza )
				this.stepFrequencyCounter = 0 ;
		}
		
		//imagine del personnagio 
		ico = new ImageIcon(getClass().getResource(str));
		img = ico.getImage();
		
		return img;
	
	}
	
	public Image Salto(){
		
		ImageIcon ico ;
		Image img;
		String str ;
		
		this.compteurSaut++;
		
		//salita
		if(this.compteurSaut <= 41){
			if(this.getInitialYPosition() > Main.scene.getHauteurPlafond())
				this.setInitialYPosition(this.getInitialYPosition() - 4);
			else this.compteurSaut = 42 ;
			if(this.isMovingToRight() == true)
				str = "/images/marioJumpRight.png";
			else str = "/images/marioJumpLeft.png";
			
			//discesa 
		}else if (this.getInitialYPosition() + this.getHeight() < Main.scene.getySol()){
			this.setInitialYPosition(this.getInitialYPosition() + 1);
			if(this.isMovingToRight() == true)
				str = "/images/marioJumpRight.png";
			else str = "/images/marioJumpLeft.png";
			
			// salto finito
		}else {
			if(this.isMovingToRight() == true)
				str = "/images/marioRightStepClose.png";
			else str = "/images/marioLeftStepClose.png";
			this.salto = false;
			this.compteurSaut = 0 ;	
		}
		//reinitialisazione img mario
		ico = new ImageIcon(getClass().getResource(str));
		img = ico.getImage();
		return img ;
				
	}
	
	public void contact (GameObjects obj){
		//contact orizontale
		if(super.contactOnTheRight(obj)== true && this.isMovingToRight()==true ||
				 (super.contactOnTheLeft(obj)==true)&& this.isMovingToRight()== false ){
			Main.scene.setMov(0);
			this.setMoving(false);
		}
		
		//contact con oggetti sotto
		if(super.contactDown(obj)==true && this.salto == true) { // mario salta su un oggetto
			Main.scene.setySol(obj.getY());
		}else if (super.contactDown(obj)==false){ // mario cade sul pavimento
			Main.scene.setySol(293);  // 293 che � il valore iniziale 
			if(this.salto == false){this.setInitialYPosition(243); // altezza iniziale di mario
		}
		
		// contact con un oggetto sopra
		if(contactUp(obj) == true){
			Main.scene.setHauteurPlafond(obj.getY() + obj.getH()); // il nuovo cielo diventa il sotto del oggetto
		}else if (super.contactUp(obj) == false && this.salto == false){
			Main.scene.setHauteurPlafond(0); // cielo iniziale
		}	
		}
	}
	
	public boolean contactPiece(Piece piece){
		// si controla avanti indietro , a destra e a sinistra
		if(this.contactOnTheLeft(piece) == true || this.contactUp(piece) == true || this.contactOnTheRight(piece)==true
				||this.contactDown(piece) == true){
			return true;
		}else return false;
	}
	
	public void contact(Characters pers){
		if((super.contactOnTheRight(pers) == true) || (super.contactOnTheLeft(pers) == true)){
			if(pers.isAlive == true){
				this.setMoving(false);
		    	this.setAlive(false);
			}
			else this.isAlive = true;
		}else if(super.contactDown(pers) == true){
			pers.setMoving(false);
			pers.setAlive(false);
		}
	}
}
