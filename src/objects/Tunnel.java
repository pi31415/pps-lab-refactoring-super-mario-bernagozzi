 package objects;

import javax.swing.ImageIcon;

public class Tunnel extends GameObjects {

	public Tunnel(int X, int Y) {
		
		super(X, Y, 43 ,65);
		super.icoObj = new ImageIcon(getClass().getResource("/images/tunello.png"));
		super.imgObj = super.icoObj.getImage();
	}

}
