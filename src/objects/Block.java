package objects;

import javax.swing.ImageIcon;

public class Block extends GameObjects {
	
	public Block(int X, int Y) {
		
		super(X, Y, 30 ,30);
		super.icoObj = new ImageIcon(getClass().getResource("/images/block.png"));
		super.imgObj = super.icoObj.getImage();
	}

	
}
